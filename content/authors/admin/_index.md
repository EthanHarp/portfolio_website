---
# Display name
title: Ethan Harpster

# Full name (for SEO)
first_name: Ethan
last_name: Harpster

# Status emoji
# status:
#   icon: ☕️

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Computer Science and Engineering Student

# Organizations/Affiliations to show in About widget
organizations:
  - name: Ohio State University
    #url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
#bio: My research interests include distributed robotics, mobile computing and programmable matter.

# Interests to show in About widget
interests:
  - Artificial Intelligence
  - Robotics
  - Computer Vision

# Education to show in About widget
education:
  courses:
    - course: BS in Computer Science and Engineering
      institution: Ohio State University
      year: 2020-2023
    # - course: MEng in Artificial Intelligence
    #   institution: Massachusetts Institute of Technology
    #   year: 2009
    # - course: BSc in Artificial Intelligence
    #   institution: Massachusetts Institute of Technology
    #   year: 2008

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:ethanharp12@protonmail.com'
  # - icon: twitter
  #   icon_pack: fab
  #   link: https://twitter.com/GeorgeCushen
  #   label: Follow me on Twitter
  #   display:
  #     header: true
  # - icon: graduation-cap # Alternatively, use `google-scholar` icon from `ai` icon pack
  #   icon_pack: fas
  #   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
  - icon: github
    icon_pack: fab
    link: https://codeberg.org/EthanHarp
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/ethanharpster
  # Link to a PDF of your resume/CV.
  # To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  # - icon: cv
  #   icon_pack: ai
  #   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true
---

Currently studying computer science and engineering at the Ohio State University with a specialization in artificial intelligence. My main interests are machine learning and robotics. I currently am the chief technology officer for the Artificial Intelligence Club, and am a member of the Machijaru research lab. My research involves using robotics and AI to automate a lab space.
{style="text-align: justify;"}
