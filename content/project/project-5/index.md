---
title: Nix/NixOS/Nixlang
summary: work in progress
tags:
  - test
date: '2022-08-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: ''

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:
  # - icon: twitter
  #   icon_pack: fab
  #   name: Follow
  #   url: https://twitter.com/georgecushen
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

I have been involved with robotics for a long time but this was my first time doing a personal robotics project. The first challenge was deciding all of the parts I needed. Due to issues with microprocessors being out of stock, I decided to use a Raspberry Pi 3 I already owned with an Adafruit motor hat. I used an original Raspberry Pi camera as that allowed me to plug in directly with CSI. I was able to find a battery that had a 5V USB out and 12V DC barrel jack out, allowing me to power the Pi and motors with the same battery. For the motors, I found a kit on amazon that had were 12V, had encoders attached, and came with a mounting bracket. For the design of the body, I was able to work with a friend from my high school robotics team to nail down the design. The first design was pretty much just a box, we found this was uneccessarily big and was difficult to correctly mount the motors. The final revision ended up being slimmer, shorter, and has cutouts for everything that needed to be mounted.

The Software
====
Designing the software was much easier for me than designing the robot. I turned the images captured from the camera into black and white, applied a binary threshold to single out the strip of blue tape, then find contours based on that. I filter the contours based on their area, then find the average point of all contours. I compare the x position of that to the center of the camera, if it is too far to one side it then corrects by slowing down the appropriate motor.

Results
====
I am fairly happy with the end result, it does work and it provides a great platform to build future projects off of. I do wish to improve the reliability of the software, it is currently very dependent on the lighting of the area. Two ideas I have to improve this is to change the position of the camera so it is pointing straight down to prevent upcoming turns from impacting it. I also would like to switch the binary threshold to a color threshold, I think this would help to accommodate more lighting situations.

Next Steps
====
I would like to upgrade the hardware (possibly an Nvidia Nano Next) to be able to run Ubuntu 20.04+. This would allow me to build a new codebase based around using ROS2. I would also like to offload any code relating to the motors to a microcontroller running FreeRTOS with microROS. This would give me a space to put more time critical actions rather than cramping everything into the microprocessor. Once I get the whole system using ROS2, I can use all of the code I wrote for facial detection and recognition.

<video controls="controls" muted="muted" class="d-block rounded-bottom-2 width-fit" style="max-height:640px;">
  <source src="robot.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

