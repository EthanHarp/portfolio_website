---
title: Facial Detection and Recognition
summary: A ROS package performing facial detection and recognition
tags:
  - ML
date: '2022-07-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: ''

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:
  # - icon: twitter
  #   icon_pack: fab
  #   name: Follow
  #   url: https://twitter.com/georgecushen
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

Tools Used
====
Python, ROS2, Pytorch, MTCNN, FaceNet, MatPlotLib, Numpy

Code
====
This project is pulled from a ROS workspace and cannot run outside of it. It also utilizes custom interface for the service. You can check it out [here](https://codeberg.org/EthanHarp/face_recdet 'Git Repo').

Overview
====
I set up my detection and recognition code as two separate ROS nodes. Then I use a ROS service to facilitate communication between the two nodes. The way it works is the detection node is a client that sends a request through the service, this then prompts a response from the server which is the recognition node. This response is fed back through the service to the client. The purpose of this project is to make a pipline that can recognize my face.

Detection
====
This node utilizes MTCNN to get a bounding box for a face in a supplied image. Then I crop to that bounding box and save that face as a new image. After everything is done, it sends a request through the ROS service to start recognition.

Recognition
====
This node utillizes FaceNet that I performed transfer learning on with my own Pytorch dataset. After it receives a request from the recognition node it feeds the image through the model, then says whether it is myself as a response back through the service.

Results
====
I am fairly happy with the quality of this project. I think the quality of the data I used could definitely be improved as I found that if I test pictures of myself with glasses or without facial hair it suffers. I am very happy with how the ROS aspect of this project turned out, it works well and can be incorporated easily into any future project I do in the future that uses ROS.

Takeaways
====
I was able to learn a lot more about ROS which was a big reason for doing this project. Also, I was able to utilize a lot of cool tools like MTCNN and FaceNet. I do think there are several ways I can improve this project for the future. First, I would make a more expansive dataset to perform transfer learning on. I also think the ROS portion could be improved by including a launch file for easier startup, and switchin from a service to an action to help with repeatability.
